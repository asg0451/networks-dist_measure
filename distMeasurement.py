#!/usr/bin/env python3

import socket, sys, time, struct, os
from random import choice
from select import select
from string import ascii_uppercase


# globals
times_retried = 0
plot = False

# parameters
DATA_LENGTH = 1458 # the length of data to make our UDP request exactly 1500 bytes
PORT = 60002 # a nonsense port that (hopefully) nothing is using on the target.
TIMEOUT = 2 # seconds


# does the actual work. sends a UDP socket, listens for an icmp error, returns data
def getStats(dest, dest_port=PORT, timeout=TIMEOUT, payload=b''):
    # new raw UDP socket
    with socket.socket(socket.AF_INET,
                       socket.SOCK_RAW,
                       socket.IPPROTO_UDP) as conn:

        # set initial ttl as per assignment
        ttl_initial = 32
        conn.setsockopt(socket.IPPROTO_IP, socket.IP_TTL, ttl_initial)

        # make udp header
        src_port = 4242 # doesn't matter
        length = 8 + len(payload)
        checksum = 0
        udp_header = struct.pack('!HHHH',
                                 src_port,
                                 dest_port,
                                 length,
                                 checksum)

        # listen for icmp error response using a new icmp socket
        with socket.socket(socket.AF_INET,
                           socket.SOCK_RAW,
                           socket.IPPROTO_ICMP) as listen:

            # send udp packet
            conn.sendto(udp_header+payload, (dest, dest_port))

            # start timeout and rtt timer
            start = time.time()

            # use select to poll the listening socket for a icmp response
            while select([listen], [], [], max(0, start + timeout - time.time()))[0]:

                # receive 2048 bytes. this should be enough
                data = listen.recvfrom(2048)
                # 'data' has some other info we dont need
                msg = data[0]
                # msg[28] is the offset to the copy of the req datagram
                datagram_offset = 28
                # check that the dest port in the icmp error's copy of our request
                # is the same (so we don't get other application's responses)
                # locations found with wireshark
                received_dest_port = int.from_bytes(msg[50:52], byteorder='big')
                if received_dest_port == dest_port:
                    newttl = msg[36]
                    return ("%.4f" % (time.time() - start), ttl_initial - newttl, len(msg)-datagram_offset)
                # no response or not for us :( return error
            return (-1, -1, -1)

# returns a random "bytes" object of given length
def randbytestring():
    return ''.join(choice(ascii_uppercase)
                   for i in range(DATA_LENGTH)).encode('ascii')

# execute the request/response for a destination and handle the result/retry
def trydest(dest):
    global times_retried

    # perform the actual req/resp action
    (rtt, steps, bytes_retained) = getStats(dest,
                                            dest_port=PORT, # unreachable port
                                            timeout=TIMEOUT, # seconds
                                            payload=randbytestring())

    # error case. retry twice before giving up
    if rtt == -1 or bytes_retained == -1 or steps == -1:
        print('error on ' + dest)
        if times_retried < 2:
            print('retrying')
            times_retried += 1
            trydest(dest)
        else:
            print('giving up on ' + dest)
            print('----------------')

    else:
        times_retried = 0
        print('rtt: ' + str(rtt))
        print('hops: ' + str(steps))
        print('bytes retained: ' + str(bytes_retained) + ' out of 1500')
        print('----------------')

        # in plot mode this function will append these values to a data file
        if plot:
            append_to_datfile(steps, rtt) # hops x, rtt y

# append values to data.dat for plotting
def append_to_datfile(x, y):
    with open('data.dat', "a") as f:
        f.write(str(x) + ' ' + "%.3f" % y + '\n')


if __name__ == '__main__':
    # are we root/sudo?
    if os.geteuid() != 0:
        exit("You need to have root privileges to run this script.")

    # are we in plot mode?
    if len(sys.argv) > 1 and sys.argv[1] == 'plot':
        plot = True
        print('plot enabled')
        # blank out plot data file
        open('data.dat', 'w').close()

    # read targets.txt and probe each target therein
    with open('targets.txt') as f:
        lines = f.readlines()
        for line in lines:
            host = line.rstrip()
            print('trying ' + host)
            trydest(host)
