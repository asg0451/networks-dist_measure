set xlabel "Hops"
set ylabel "RTT"
set term png
set output "hops_rtt.png"
f(x) = m*x+k
m = 1; k = -0.1
limit = 0.01
fit f(x) "data.dat" via m,k
fit_label = sprintf("f(x) = %.3fx+%.2f", m, k)
plot "data.dat" with points pointtype 5 t "RTT/Hop data", f(x) t fit_label
